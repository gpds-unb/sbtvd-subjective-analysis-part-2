#!/usr/bin/env python3

import argparse
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.optimize as opt
import seaborn as sns
from absl import app
from absl.flags import argparse_flags
from sklearn.preprocessing import MinMaxScaler


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--input', type=str,
                        help='Path for the processed/unscrambled scores (output_scores.csv).',
                        default='../data/output_scores.csv')
    parser.add_argument('--output', type=str,
                        help='Path of the processed file with the MOS and SOS per video.',
                        default='../data/MOS_SOS.csv')
    parser.add_argument('--sos_curve', type=str,
                        help='Path of the processed file with the MOS and SOS per video.',
                        default='../plots/SOS.pdf')
    parser.add_argument('--scores_histogram', type=str,
                        help='Path of the processed file with the histogram of scores.',
                        default='../plots/score_histogram.pdf')
    parser.add_argument('--scores_boxplot', type=str,
                        help='Path of the processed file with the boxplot of scores.',
                        default='../plots/pervideo_boxplot.pdf')
    parser.add_argument('--scores_violinplot', type=str,
                        help='Path of the processed file with the violinplot of scores.',
                        default='../plots/pervideo_violinplot.pdf')
    args = parser.parse_args(argv[1:])
    return args


def compute_metric(df, metric=np.mean):
    column_names = [col for col in df if col.startswith('vc-')]
    dict_temp = {}
    for column in column_names:
        dict_temp[column] = metric(df[column])
    df_temp_mos = pd.DataFrame([dict_temp])
    return df_temp_mos


def compute_mos(df):
    return compute_metric(df, metric=np.nanmean)


def compute_sos(df):
    return compute_metric(df, metric=np.nanstd)


def compute_metrics(df, args):
    df_mean = compute_mos(df)
    df_sos = compute_sos(df)
    df_median = compute_metric(df, metric=np.nanmedian)
    output = []
    for column in df_mean:
        output.append({
            "Video": column, "MOS": df_mean[column].values.squeeze(),
            "SOS": df_sos[column].values.squeeze(),
            "Median": df_median[column].values.squeeze()
        })
    return pd.DataFrame(output)

def compute_multiple_metrics(df, args):
    df_final = compute_metrics(df, args)
    df_final.to_csv(args.output, index=False)


def compute_scores_histogram(df, args):
    column_names = [col for col in df if col.startswith('vc-')]
    all_columns = []
    for column in column_names:
        all_columns.append(df[column])
    all_samples = np.concatenate(all_columns, axis=0)
    dirname = os.path.dirname(args.scores_histogram)
    os.makedirs(dirname, exist_ok=True)
    _ = plt.hist(all_samples, bins='auto')
    plt.title("Histogram of the individual scores")
    plt.savefig(args.scores_histogram, bbox_inches="tight")

def generate_SOS_curve(df_in, args):
    def model_f(x, a):
        return a * (-x**2 + 6 * x - 7)
    scaler = MinMaxScaler(feature_range=(0, 6))
    df = df_in.copy()
    column_names = [col for col in df if col.startswith('vc-')]
    df[column_names] = scaler.fit_transform(df[column_names])
    df_mean = compute_mos(df)
    df_sos = compute_sos(df)
    output = []
    for column in df_mean:
        output.append({
            "Video": column,
            "MOS": df_mean[column].values.squeeze(),
            "SOS": df_sos[column].values.squeeze(),
        })
    df_final = pd.DataFrame(output)
    MOS_qr = np.array(df_final.MOS.values)
    SOS_qr = np.array(df_final.SOS.values)
    params, _ = opt.curve_fit(model_f, xdata=MOS_qr, ydata=SOS_qr, p0=[0])
    a_opt= params
    x_model = np.linspace(0, 6, 100)
    y_model = model_f(x_model, a_opt)
    plt.scatter(MOS_qr, SOS_qr)
    plt.plot(x_model, y_model, color='r')
    plt.xlim([0, 6])
    plt.ylim([0, 2])
    plt.savefig(args.sos_curve, bbox_inches="tight")


def plot_generator(df, input_filename, title, ax_function):
    suffixes = ['-321-', '-31-', '-322-']
    for suffix in suffixes:
        column_names = [col for col in df if
                        (col.startswith('vc-') and col.endswith(suffix))]
        dataframe = df[column_names]
        sns.set(style="whitegrid")
        plt.figure(figsize=(10, 6))
        ax = ax_function(dataframe)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
        plt.title(title)
        plt.ylabel("Score")
        plt.xlabel("Video")
        dirname = os.path.dirname(input_filename)
        os.makedirs(dirname, exist_ok=True)
        basename = os.path.basename(input_filename)
        out_name = f"{dirname}/VUT_{suffix}_{basename}"
        plt.savefig(out_name, bbox_inches="tight")


def generate_score_pervideo_boxplot(df, args):
    ax_fun = lambda x: sns.boxplot(
        data=x, orient="v", palette="Set2", notch=True
    )
    plot_generator(
        df=df,
        input_filename=args.scores_boxplot,
        title="Boxplot of scores per video.",
        ax_function=ax_fun
    )


def generate_score_pervideo_violinplot(df, args):
    ax_fun = lambda x: sns.violinplot(
        data=x, orient="v", palette="Set2", inner="quart"
    )
    plot_generator(
        df=df,
        input_filename=args.scores_violinplot,
        title="Violinplot of scores per video.",
        ax_function=ax_fun
    )


def main(args):
    df = pd.read_csv(args.input)
    generate_SOS_curve(df, args)
    generate_score_pervideo_boxplot(df, args)
    generate_score_pervideo_violinplot(df, args)
    compute_scores_histogram(df, args)
    compute_multiple_metrics(df, args)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
