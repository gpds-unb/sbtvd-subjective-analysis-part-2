#!/usr/bin/env python3

import argparse
import tqdm
import pandas as pd
from absl import app
from absl.flags import argparse_flags


def parse_args(argv):
    parser = argparse_flags.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '--input', type=str, help='Path for raw_scores.csv.',
        default='data/raw_scores.csv'
    )
    parser.add_argument(
        '--output', type=str, help='Path of the output (processed) file.',
        default='data/output_scores.csv'
    )
    args = parser.parse_args(argv[1:])
    return args


def get_seq1_reordering():
    return {
        'V1': 'vc-philips-01_VUT-321-',
        'V2': 'vc-globo-01_VUT-321-',
        'V3': 'vc-globo-05_VUT-321-',
        'V4': 'vc-lcevc-01_VUT-321-',
        'V5': 'vc-philips-03_VUT-321-',
        'V6': 'vc-globo-01_VUT-322-',
        'V7': 'vc-globo-05_VUT-31-',
        'V8': 'vc-philips-03_VUT-322-',
        'V9': 'vc-lcevc-01_VUT-322-',
        'V10': 'vc-philips-01_VUT-31-',
        'V11': 'vc-globo-01_VUT-31-',
        'V12': 'vc-lcevc-01_VUT-31-',
        'V13': 'vc-philips-03_VUT-31-',
        'V14': 'vc-philips-01_VUT-322-',
        'V15': 'vc-globo-05_VUT-322-'
    }


def get_seq2_reordering():
    return {
        'V1': 'vc-globo-05_VUT-321-',
        'V2': 'vc-lcevc-01_VUT-321-',
        'V3': 'vc-globo-01_VUT-321-',
        'V4': 'vc-philips-01_VUT-321-',
        'V5': 'vc-philips-03_VUT-321-',
        'V6': 'vc-globo-05_VUT-31-',
        'V7': 'vc-globo-01_VUT-31-',
        'V8': 'vc-philips-03_VUT-31-',
        'V9': 'vc-globo-01_VUT-322-',
        'V10': 'vc-philips-01_VUT-322-',
        'V11': 'vc-globo-05_VUT-322-',
        'V12': 'vc-philips-03_VUT-322-',
        'V13': 'vc-lcevc-01_VUT-322-',
        'V14': 'vc-philips-01_VUT-31-',
        'V15': 'vc-lcevc-01_VUT-31-'
    }


def get_seq3_reordering():
    return {
        'V1': 'vc-lcevc-01_VUT-321-',
        'V2': 'vc-philips-03_VUT-321-',
        'V3': 'vc-globo-01_VUT-321-',
        'V4': 'vc-globo-05_VUT-321-',
        'V5': 'vc-philips-01_VUT-321-',
        'V6': 'vc-globo-01_VUT-31-',
        'V7': 'vc-globo-05_VUT-322-',
        'V8': 'vc-philips-01_VUT-322-',
        'V9': 'vc-philips-03_VUT-31-',
        'V10': 'vc-lcevc-01_VUT-322-',
        'V11': 'vc-globo-01_VUT-322-',
        'V12': 'vc-globo-05_VUT-31-',
        'V13': 'vc-philips-01_VUT-31-',
        'V14': 'vc-lcevc-01_VUT-31-',
        'V15': 'vc-philips-03_VUT-322-'
    }


def get_seq4_reordering():
    return {
        'V1': 'vc-philips-01_VUT-321-',
        'V2': 'vc-lcevc-01_VUT-321-',
        'V3': 'vc-philips-03_VUT-321-',
        'V4': 'vc-globo-01_VUT-321-',
        'V5': 'vc-globo-05_VUT-321-',
        'V6': 'vc-philips-03_VUT-322-',
        'V7': 'vc-philips-01_VUT-31-',
        'V8': 'vc-globo-05_VUT-322-',
        'V9': 'vc-globo-01_VUT-31-',
        'V10': 'vc-philips-01_VUT-322-',
        'V11': 'vc-globo-01_VUT-322-',
        'V12': 'vc-globo-05_VUT-31-',
        'V13': 'vc-lcevc-01_VUT-322-',
        'V14': 'vc-philips-03_VUT-31-',
        'V15': 'vc-lcevc-01_VUT-31-'
    }


def get_seq_reordering(sequence_number):
    switcher = {
        1: get_seq1_reordering,
        2: get_seq2_reordering,
        3: get_seq3_reordering,
        4: get_seq4_reordering
    }
    return switcher.get(sequence_number, 0)()


def unify_scores_regardless_pseudosequence(df):
    sequence_ids = sorted(df.SEQ.unique())
    out = []
    for sequence in sequence_ids:
        df_temp = df.loc[df['SEQ'] == sequence]
        rename = get_seq_reordering(sequence)
        df_temp = df_temp.rename(columns=rename)
        out.append(df_temp)
    df_out = pd.concat(out)
    return df_out.sort_values(by=['UID'])


def convert_categorical_to_numeric(category):
    switcher = {
        'muito pior': -3,
        'pior': -2,
        'pouco pior': -1,
        'igual': 0,
        'pouco melhor': 1,
        'melhor': 2,
        'muito melhor': 3
    }
    return switcher.get(category.lower(), None)


def convert_scale_to_numeric(df):
    filter_col = [col for col in df if col.startswith('vc-')]
    for col in tqdm.tqdm(filter_col):
        df[col] = df[col].map(lambda x: convert_categorical_to_numeric(x))
    return df


def main(args):
    df = pd.read_csv(args.input)
    df = unify_scores_regardless_pseudosequence(df)
    df = convert_scale_to_numeric(df)
    df = df.drop(columns=['SEQ'])
    df.to_csv(args.output,index=False)


if __name__ == "__main__":
    app.run(main, flags_parser=parse_args)
